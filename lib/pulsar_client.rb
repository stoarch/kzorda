# encoding: utf-8

require 'json'
require 'open-uri'

# Read data from remote service as json and return it as meter
class PulsarClient
	def initialize
		init_meters
	end

	def init_meters
		@meters = {
			 'Гостиница КТС' => 155084,
			 'Баня Димаш' => 155093,
			 'Педагогический колледж' => 155086 
		 }
	end

	def read_meters()
		res = open("http://10.168.111.10:4567").read
		JSON.parse(res)
	rescue 
		$log.error $!
	end

	def read_vals(date_start, date_end)
		res = open("http://178.91.94.13:4567/data/between?date_start=#{date_start}&date_end=#{date_end}").read
		JSON.parse(res)
	rescue 
		ap $!
		$log.error $!
	end

	def read_vals_grouped_by_meters(date_start, date_end)
		vals = read_vals(date_start, date_end)
		vals.group_by{|v|v["Name"]}.values unless vals.nil?
	end

	def read_meter_codes()
		meters = read_meters
		meter_codes = []
		return [] if meters.nil? || meters == true
		meters.each do |m|
			meter_codes << fill_meter([m])
		end
		meter_codes
	end

	def read_meters_vals( date_start, date_end )
		vals = read_vals_grouped_by_meters(date_start, date_end)
		meters = []
		vals.each do |v|
			meters << fill_meter( v ) 
		end
		meters
	end

	def fill_meter( v )
		meter = {	counter_id: '?', code: 0, value: 0.0, datetime: nil, caption: '?', factor: 0.01, values: []}

		if( v.size > 0 )
			meter_caption = v[0]["Name"]
			meter[:code] = get_code( v[0]["plc_id"], v[0]["prp_id"] ) 
			meter[:caption] = meter_caption
			meter[:group] = v[0]["NameGroup"]
			meter[:typ_arh] = v[0]["typ_arh"]
		
			meter[:values] = fill_meter_values( v ) unless v[0]["DateValue"].nil?
		end
		meter
	end

	def get_code( plc_id, prp_id )
		plc_id*10000 + prp_id
	end

	def fill_meter_values( v )
		res = []
		v.each do |mv|
			unless( mv["DataValue"].nil? and mv["DateValue"].nil?)
				date = mv["DateValue"]
				date ||= DateTime.now.to_s
				res << {read: mv["DataValue"], tbrtime: DateTime.parse(date)}  	
			end
		end
		res
	end

=begin 
FROM SERVICE
=> [[{"DateValue"=>"2016-05-03 12:05:45 +0600",
   "Name"=>
    "\u0414\u0435\u0442\u0441\u043A\u0438\u0439 \u0441\u0430\u0434 \u0421\u0430\
u043C\u0430\u043B",
   "typ_arh"=>0,
   "DataValue"=>2.0},
  {"DateValue"=>"2016-05-03 12:08:10 +0600",
   "Name"=>
    "\u0414\u0435\u0442\u0441\u043A\u0438\u0439 \u0441\u0430\u0434 \u0421\u0430\
u043C\u0430\u043B",
   "typ_arh"=>0,
   "DataValue"=>2.0},


FROM DB:
=> [{:counter_id=>"F0001214",
  :code=>770525,
  :value=>0.0,
  :datetime=>nil,
  :caption=>
   "\u0421\u043F\u043E\u0440\u0442 \u043A\u043E\u043C\u043F\u043B\u0435\u043A\u0
441 Petro Kaz - \u0443\u043B. \u0410\u0431\u0430\u044F \u0431.\u043D.",
  :factor=>0.01,
  :values=>
   [{:read=>1391.56, :tbrtime=>2016-05-05 18:00:50 +0600},
=end

end

# encoding: utf-8

require 'awesome_print'
require 'sinatra'
require 'sinatra/base'
require 'sinatra/contrib'
require 'net/http'
require 'uri'
require 'time_difference'
require 'sequel'
require 'prawn'
require 'prawn/measurement_extensions'
require 'prawn/table'
require 'csv'
require 'pry'
require 'logger'

require_relative 'pulsar_client'

$log = Logger.new 'kzorda.log', 'daily'

class Application < Sinatra::Base
    enable :sessions

    use Rack::Session::Cookie, secret: 'Fi5ty thou$and monkey pu1l banana in it a5s'

    set :static, true
    set :root, File.dirname(__FILE__)

    configure :development do
        register Sinatra::Reloader
    end

	unless development?
		disable :show_exceptions
	end 

	configure do
		set :public_folder, File.dirname(__FILE__) + '/static' 

	end

	before do
		expires 600, :public, :must_revalidate
	end


### ROUTES ###
	get '/' do
		@meters = init_meters
		read_pulsar_codes
		@meter_date_start ||= DateTime.now - 1
		@meter_date_end ||= DateTime.now 
		erb :main
	end
	
	get '/meters.json' do
		@meters = init_meters()

		parse_meter_report_type(params)

		parse_date_range( params[:st_date], params[:en_date] )

		@meter_date_start ||= DateTime.now - 31
		@meter_date_end ||= DateTime.now 

		read_meter_vals( @meters )

		meters_by_name = []
		if( params[:meter] && params[:meter] != '' )
			re = Regexp.new params[:meter]
			meters_by_name = @meters.select{|v| v[:caption] =~ re unless v[:caption].nil?}
			@meters = meters_by_name 
		end

		@meters = filter_meter_vals( @meters, @meter_report_type )
		@meters.to_json
	end

	get '/meters' do
		@meters = init_meters()

		parse_meter_report_type(params)

		parse_date_range( params[:st_date], params[:en_date] )

		@meter_date_start ||= DateTime.now - 31
		@meter_date_end ||= DateTime.now 

		read_meter_vals( @meters )

		meters_by_name = []
		if( params[:meter] && params[:meter] != '' )
			re = Regexp.new params[:meter]
			meters_by_name = @meters.select{|v| v[:caption] =~ re unless v[:caption].nil?}
			@meters = meters_by_name 
		end

		@meters = filter_meter_vals( @meters, @meter_report_type )
		erb :meters 
	end

	get '/meter.csv' do

		init_meters()
		parse_meter_report_type(params)

		meters = $meters.select{|m| m[:code] == params[:code].to_i}

		if meters.size == 0 
			return [400, "Не найден счётчик"]
		end

		meter = meters[0]

		@meters = [{code: params[:code].to_i, value:0, datetime:nil, factor: meter[:factor], caption: meter[:caption], counter_id: meter[:counter_id] }]

		parse_date_range( params["st_date"], params["en_date"] )

		read_meter_vals( @meters )
		@meters = filter_meter_vals( @meters, @meter_report_type )

		file_name = "meter#{params[:code]}.csv"
		generate_meters_csv(file_name)

		send_file file_name 
	end

	get '/meters.csv' do
		@meters = init_meters()

		parse_meter_report_type(params)
		parse_date_range( params["st_date"], params["en_date"] )

		read_meter_vals( @meters )
		@meters = filter_meter_vals( @meters, @meter_report_type )

		generate_meters_csv()

		send_file "meters.csv"
	end

	get '/meter.pdf' do

		pcodes = read_pulsar_codes()
		init_meters()
		$meters.concat pcodes 

		parse_meter_report_type(params)


		meters = $meters.select{|m| m[:code] == params[:code].to_i}

		if meters.size == 0 
			return [400, "Не найден счётчик"]
		end

		meter = meters[0]

		@meters = []

		parse_date_range( params["st_date"], params["en_date"] )

		read_meter_vals( @meters )
		pcode = params[:code].to_i
		@meters.select!{|m| m[:code] == pcode }
		@meters = filter_meter_vals( @meters, @meter_report_type )


		file_name = "meter_#{params[:code]}.pdf"

		if( @meters[0][:values].size > 0 )
			generate_meters_pdf(file_name) 
			send_file file_name 
		else
			[400, 'Нет данных']
		end

	end

	get '/meters.pdf' do

		@meters =	init_meters()

		parse_meter_report_type(params)
		parse_date_range( params["st_date"], params["en_date"] )

		read_meter_vals( @meters )
		@meters = filter_meter_vals( @meters, @meter_report_type )
		generate_meters_pdf()

		send_file "meters.pdf"
	end

	## APP METHODS ##
	def parse_date_range( date_start, date_end )
	begin
			@meter_date_start = DateTime.parse( date_start ) unless ( date_start.nil? )
			@meter_date_end = DateTime.parse( date_end ) unless ( date_end.nil? )
	rescue
	end
	end

	def parse_meter_report_type(params)
		@meter_report_type ||= :all_vals

		unless( params[:report_type].nil? )
			@meter_report_type = params[:report_type].to_sym
		end
	end
end


### UTILITY ###
def read_cpoint_vals( cpoints )
	db = Sequel.tinytds( user: 'puser', password: 'Win3Ts', host: '92.47.27.114', database: 'concentrator', encoding: 'cp1251')


	db.disconnect
end

def read_meter_vals( meters )

		@pulsar_meters =  PulsarClient.new.read_meters_vals(@meter_date_start.strftime('%d.%m.%Y'),@meter_date_end.strftime('%d.%m.%Y'))
		meters.concat @pulsar_meters
end

def filter_meter_vals( meters, report_type )
	case report_type
		when :all_vals 
			meters #do nothing 
		when :hourly_vals 
			meters.each do |m|
				res = []
				m[:values] = m[:values].group_by{|v| v[:tbrtime].to_date}.map{|v| v[1][0]}
			end
			meters
		when :monthly_vals
			meters.each do |m|
				res = []
				m[:values] = m[:values].group_by{|v| [v[:tbrtime].to_date.year, v[:tbrtime].to_date.month]}.map{|v| v[1][0]}
			end
			meters
	end
end

$meters_info = {
 30002 => {caption:"Пед колледж"},
 40003 => {caption:"КТС"},
 50004 => {caption:"ИП Баймурзаева"},
 60005 => {caption:"Детский сад Самал"},
 70006 => {caption:"Детский сад Умай"},
 80011 => {caption:"Реабилитационный центр №2"},
 100014 => {caption:"Кузет"},
 120015 => {caption:"Автомойка Турбо"},
 130016 => {caption:"Пожарная часть №11"},
 140017 => {caption:"ТОО ДГУ Сервис"},
 170021 => {caption:"Кафе Карьяна"},
 190023 => {caption:"Автомойка Арай"},
 200024 => {caption:"Автомойка CHISTO"},
 210025 => {caption:"Саутс ойл"},
 220026 => {caption:"Авто-Рай"}

}

def init_meters
		@pulsar_meters =[] 

		$meters = [
			{counter_id: 'F0001214', code: 770525, value: 0.0, datetime: nil, caption: 'Спорт комплекс Petro Kaz - ул. Абая б.н.', factor: 0.01}
			]
end

def read_pulsar_codes
	@pulsar_meters =  PulsarClient.new.read_meter_codes
end

ColumnWidths = [100, 150, 100, 80]
Headers = ['Код',  'Дата', 'Значение', 'Расход']

def report_row( pdf, code, date, value, flow)
	row = [code, date.strftime("%d-%m-%Y %H:%M"), value.round(2), flow.round(2)] 
	pdf.make_table([row]) do |t|
		t.column_widths = ColumnWidths
		t.cells.style borders: [:left, :right], padding: 2
	end
end

def generate_meters_pdf( file_name = "meters.pdf" )
		Prawn::Document.generate(file_name) do |pdf|
			pdf.font_families.update(
						"Verdana" => {
							:bold => "./static/fonts/verdanab.ttf",
							:italic => "./static/fonts/verdanai.ttf",
							:normal  => "./static/fonts/verdana.ttf" })
			
			pdf.font "Verdana", :size => 12

			pdf.text "Отчёт по расходомерам", align: :center, size: 18
			pdf.move_down 1.cm

			pdf.text "Кызылорда Су Жуйесi", align: :center, size: 14
			pdf.move_down 0.5.cm

			pdf.text "C #{@meter_date_start.strftime('%d-%m-%Y')} по #{@meter_date_end.strftime('%d-%m-%Y')}", align: :center

			data = []
			@meters.each do |m|
				sum_flow = 0
				prev = -1 

				pdf.move_down 1.cm
				pdf.text "Расходомер #{m[:counter_id]}"
				pdf.text "#{m[:caption]}", size: 16
				pdf.move_down 0.5.cm


				data = []
				m[:values].reverse.each_with_index do |v,i|
					prev = v[:read] if prev < 0 
					data << report_row(pdf, i, v[:tbrtime], v[:read], v[:read] - prev )
					sum_flow += v[:read] - prev 
					prev = v[:read]
				end

				head = pdf.make_table([Headers], column_widths: ColumnWidths )
				pdf.table([[head], *(data.map{|d| [d]})], header: true, row_colors: %w[cccccc ffffff]) do
					row(0).style background_color: '000000', text_color: 'ffffff'
					cells.style borders: []
				end

				pdf.move_down 0.3.cm
				pdf.text "Расход за период: #{sum_flow.round(2)}"
			end

			creation_date = DateTime.now.strftime("Отчёт сгенерирован %e %b %Y в %H:%M")
			pdf.go_to_page(pdf.page_count)
			pdf.move_down(710)
			pdf.text creation_date, align: :right, style: :italic, size: 9
		end
end


def generate_meters_csv( file_name = "meters.csv" )

	CSV.open( file_name, "wb" ) do |csv|
		csv << ["No", "Код", "Дата", "Значение", "Расход"]
		index = 1
		@meters.each_with_index do |m,i|

			old_val = -1

			csv << [index, m[:code], m[:counter_id],  m[:caption] , "" ]
			index += 1

			m[:values].each_with_index do |v,j|

				old_val = v[:read] if old_val && old_val < 0 
				csv << [index, m[:code], v[:tbrtime].strftime("%d-%m-%Y %H:%M"), v[:read].round(2), (v[:read] - old_val).round(2) ]
				old_val = v[:read]

				index += 1
			end
		end
	end		
end

